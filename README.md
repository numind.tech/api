# Docker Images for API

## TLDR

Use *NuMind NLP* desktop application to create a model, then export it via the
`export model` button located on the top right of the project window, you’ll
get a `file.model`.

Then on a container host (Linux), the following command will run the API
(after a precharging time) using custom model located in `path/file.model` and
exposing port `9000` on the host.

```
docker run -d \
  -v path/file.model:/file.model \
  -p 9000:9000 \
  registry.gitlab.com/numind.tech/api
```

Note: if the model path is relative, be sure to make it start by `./`. For
instance, if the command is run in the same directory as `file.model`, use
`./file.model` as a path or the absolute path but not just `file.model`.

## Compatibilities

**latest** is currently compatible with models created with NuMind Application
versions **1.6.25+**.

## Advanced Configuration

### Faster Startups

Because each custom model can depend on a different *Foundation Models*, they
are not packaged in this image. So before being able to infer your custom
model, the API has to download the corresponding *Foundation Models*, which
can take a few minutes.

There are two ways to get around this issue: using a permanent volume or
creating a custom image with all dependencies.

#### Permanent Volume

First create a named volume:
```
docker volume create numind
```

Then launch the API with the volume mounted on `/var/cache/numind-nlp`:
```
docker run -d \
  -v numind:/var/cache/numind-nlp \
  -v path/file.model:/file.model \
  -p 9000:9000 \
  registry.gitlab.com/numind.tech/api
```

The first launch can take a few minutes but the following launchs will be fast.

Note: you can share the same volume between any runs of NuMind API, even for
different models, even if they do not use the same *foundation model*.

#### Custom Image

To create a custom image fine-tuned to your model, first launch it in dry-run
mode:
```
docker run \
    -v path/file.model:/file.model \
    --name numind-dry-run \
    registry.gitlab.com/numind.tech/api \
    --dry-run
```

Then commit the image:
```
docker commit -c "CMD []" numind-dry-run numind/my-project
```

Finally, to launch the api, you just have to run:
```
docker run -d -p 9000:9000 numind/my-project
```

No more `-v` option for the model as it was included by the dry-run.

### Changing Port

By default, NuMind API is listening on port `9000` on all interfaces inside
its container. If you want to change the exposed port on the container host,
set a different value for `-p` like `-p 8090:9000` to expose port `8090`.

If you have a more secure settings with a proxy and do not want to expose such
port on the host, just remove the `-p` setting.
